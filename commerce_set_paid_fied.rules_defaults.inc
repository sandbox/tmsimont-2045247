<?php

/**
 * Implements hook_default_rules_configuration().
 */ 
function commerce_set_paid_field_default_rules_configuration() {
  $configs = array();
  /**
   * On order paid in full event, set field_paid_in_full value to "now"
   * Note that this requres a field_paid_in_full date field on order entities
   */
  $configs['rules_mark_date_when_order_is_paid_in_full'] = '{ "rules_mark_date_when_order_is_paid_in_full" : {
      "LABEL" : "Mark date when order is paid in full",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_payment" ],
      "ON" : [ "commerce_payment_order_paid_in_full" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "field_paid_in_full" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "commerce-order:field-paid-in-full" ], "value" : "now" } }
      ]
    }
  }';
  return $configs;
}